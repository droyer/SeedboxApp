import { Component, TemplateRef, ViewChild, Inject } from '@angular/core';
import { TorrentService } from '../_services/torrent.service';
import { Torrent } from '../_models/torrent';
import {
  BsModalRef,
  BsModalService
} from '../../../node_modules/ngx-bootstrap';
import { AppComponent } from '../app.component';

declare let alertify: any;

@Component({
  selector: 'app-torrents',
  templateUrl: './torrents.component.html',
  styleUrls: ['./torrents.component.css']
})
export class TorrentsComponent {
  @ViewChild('file')
  file;
  @ViewChild('magnet')
  magnet;

  torrents: Torrent[];
  modalRef: BsModalRef;
  removeHash: string;
  removeIndex: number;
  pbType: string;
  btnName: string;
  username: string;

  constructor(
    private torrentService: TorrentService,
    private modalService: BsModalService,
    @Inject(AppComponent) public parent: AppComponent
  ) {
    this.btnName = 'Torrent';
  }

  openModal(template: TemplateRef<any>, index: number, hash: string) {
    this.removeIndex = index;
    this.removeHash = hash;
    this.modalRef = this.modalService.show(template, {
      class: 'modal-sm modal-dialog-centered'
    });
  }

  getTorrents() {
    this.torrentService.getTorrents().subscribe((torrents: Torrent[]) => {
      this.torrents = torrents;
      this.getTorrents();
    }, () => {
      this.parent.error = true;
    });
  }

  removeTorrent() {
    this.torrentService.removeTorrent(this.removeHash).subscribe(() => {
      this.torrents.splice(this.removeIndex, 1);
      this.modalRef.hide();
      alertify.notify('Torrent supprimé', 'success', 5);
    });
  }

  addFile(file) {
    this.torrentService.addFileTorrent(file).subscribe(
      (torrent: Torrent) => {
        this.addTorrent(torrent);
      },
      () => {
        this.addTorrent(null);
      }
    );
  }

  onFileAdded() {
    const file: any = this.file.nativeElement.files[0];
    this.addFile(file);
    this.file.nativeElement.value = '';
  }

  btnTorrent() {
    if (this.btnName === 'Torrent') {
      this.file.nativeElement.click();
    } else {
      this.torrentService
        .addMagnetTorrent(this.magnet.nativeElement.value)
        .subscribe(
          (torrent: Torrent) => {
            this.magnet.nativeElement.value = '';
            this.addTorrent(torrent);
          },
          () => {
            this.addTorrent(null);
          },
          () => {
            this.onFocusOut();
          }
        );
    }
  }

  addTorrent(torrent: Torrent) {
    if (torrent === null) {
      alertify.notify('Torrent non ajouté', 'error', 5);
    } else {
      this.torrents.push(torrent);
      alertify.notify('Torrent ajouté', 'success', 5);
    }
  }

  onFocus() {
    this.btnName = 'Magnet';
  }

  onFocusOut() {
    if (this.magnet.nativeElement.value === '') {
      this.btnName = 'Torrent';
    }
  }

  setUsername(username: string) {
    this.username = username;
  }

  trackByFn(item) {
    return item.id;
  }
}
