import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { environment } from '../environments/environment';
import { SessionStorage } from '../../node_modules/ngx-store';

declare let alertify: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @SessionStorage()
  username = '';

  @ViewChild('appTorrents')
  appTorrents;

  public error: boolean;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    if (this.username === '') {
      this.http
        .post(environment.apiUrl + 'auth/login', null)
        .subscribe((user: any) => {
          this.setUsername(user.name);
          this.appTorrents.getTorrents();
        });
    } else {
      this.setUsername(this.username);
      this.appTorrents.getTorrents();
    }
  }

  onDrop(event) {
    event.preventDefault();
    if (this.username !== '') {
      const file = event.dataTransfer.files[0];
      this.appTorrents.addFile(file);
    }
  }

  onDrag(event) {
    event.preventDefault();
  }

  logout() {
    if (this.username !== '') {
      this.http.post(environment.apiUrl + 'auth/logout', null).subscribe(
        () => {},
        () => {
          this.setUsername('');
          alertify.notify('Déconnecté', 'success', 5);
          window.location.reload(true);
        },
        () => {}
      );
    }
  }

  setUsername(username: string) {
    this.username = username;
    this.appTorrents.setUsername(username);
  }

  restart() {
    if (this.username !== '') {
      this.error = false;
      this.http.post(environment.apiUrl + 'auth/restart', null).subscribe(() => {
        this.appTorrents.getTorrents();
      }, () => {
        this.error = true;
      });
    }
  }
}
