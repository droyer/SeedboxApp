export interface Torrent {
    hash: string;
    name: string;
    size: string;
    percent: number;
    download: string;
    send: string;
    ratio: number;
    reception: string;
    timeLeft: string;
}
