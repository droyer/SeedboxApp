import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from '../../../node_modules/rxjs';
import { Torrent } from '../_models/torrent';

@Injectable({
  providedIn: 'root'
})
export class TorrentService {
  url = environment.apiUrl + 'torrents/';

  constructor(private http: HttpClient) {}

  getTorrents(): Observable<Torrent[]> {
    return this.http.get<Torrent[]>(this.url);
  }

  removeTorrent(hash) {
    return this.http.delete(this.url + hash);
  }

  addFileTorrent(file) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);

    return this.http.post(this.url + 'file', formData);
  }

  addMagnetTorrent(magnet: string) {
    const obj: any = { value: magnet };

    return this.http.post(this.url + 'magnet', obj);
  }
}
