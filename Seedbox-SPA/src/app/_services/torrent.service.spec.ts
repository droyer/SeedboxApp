/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { TorrentService } from './torrent.service';

describe('Service: Torrent', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TorrentService]
    });
  });

  it('should ...', inject([TorrentService], (service: TorrentService) => {
    expect(service).toBeTruthy();
  }));
});
