<img src="https://gitlab.com/Exec57/SeedboxApp/raw/master/image.png">

# Docker

Follow this [link](https://gitlab.com/droyer/seedbox-docker).

# Seedbox

Client for rtorrent.

## Requirements

```
rtorrent
screen
dotnet runtime
nginx
```

## Users

```bash
useradd --shell /bin/bash --home /home/<username> <username>
mkdir --parents /home/<username>/{torrents,.session}
passwd <username>
```

```bash
vim /etc/ssh/sshd_config
```

```ssh
Match user <username>,<username2>,<username3>
ChrootDirectory %h
```

## rtorrent

```bash
vim /home/<username>/.rtorrent.rc
```

```ini
scgi_port = 127.0.0.1:500x
encoding_list = UTF-8
port_range = 45000-65000
port_random = no
check_hash = no
directory = /home/<username>/torrents
session = /home/<username>/.session
encryption = allow_incoming, try_outgoing, enable_retry
use_udp_trackers = yes
dht = off
peer_exchange = no
min_peers = 40
max_peers = 100
min_peers_seed = 10
max_peers_seed = 50
max_uploads = 15
schedule = espace_disque_insuffisant,1,30,close_low_diskspace=500M
```

```bash
apt-get install psmisc #killall
vim /etc/init.d/<username>-rtorrent
```

```bash
#!/usr/bin/env bash

user=<username>

rt_start() {
    su --command="screen -dmS ${user}-rtorrent rtorrent" "${user}"
}

rt_stop() {
    killall --user "${user}" screen
}

case "$1" in
start) echo "Starting rtorrent..."; rt_start
    ;;
stop) echo "Stopping rtorrent..."; rt_stop
    ;;
restart) echo "Restart rtorrent..."; rt_stop; sleep 1; rt_start
    ;;
*) echo "Usage: $0 {start|stop|restart}"; exit 1
    ;;
esac
exit 0
```

```bash
chmod +x /etc/init.d/<username>-rtorrent
cd /etc/init.d
update-rc.d <username>-rtorrent defaults
/etc/init.d/<username>-rtorrent start
```

## Service

```bash
vim /etc/systemd/system/seedbox.service
```

```ini
[Unit]
Description=Seedbox

[Service]
WorkingDirectory=/var/www/seedbox
ExecStart=/usr/bin/dotnet /var/www/seedbox/Seedbox-API.dll
Restart=always
RestartSec=10
SyslogIdentifier=Seedbox
User=root
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false

[Install]
WantedBy=multi-user.target
```

```bash
systemctl enable seedbox.service
systemctl start seedbox.service
```

## Nginx

```bash
cd /tmp
wget https://dl.eff.org/certbot-auto
chmod a+x ./certbot-auto
./certbot-auto certonly --standalone --rsa-key-size 4096
```

```bash
htpasswd -c /etc/nginx/passwd/<username>_passwd <username>
```

```bash
vim /etc/nginx/sites-enabled/seedbox
```

```nginx
server {
        listen 80;
        server_name <servername>;

        return 301 https://$host$request_uri;
}

server {
        listen 443 ssl;
        server_name <servername>;

        ssl_certificate <path_to_certificate>;
        ssl_certificate_key <path_to_private_key>;

        auth_basic "seedbox";
        auth_basic_user_file "/etc/nginx/passwd/seedbox_passwd";

        autoindex on;
        autoindex_format xml;

        location / {
                proxy_pass         http://localhost:5000;
                proxy_http_version 1.1;
                proxy_set_header   Upgrade $http_upgrade;
                proxy_set_header   Connection keep-alive;
                proxy_set_header   Host $host;
                proxy_cache_bypass $http_upgrade;
                proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header   X-Forwarded-Proto $scheme;
        }

        location /data {
                alias /home/$remote_user/torrents;
                xslt_stylesheet /root/superbindex.xslt;
        }

        location = /<USERNAME> {
                include scgi_params;
                scgi_pass 127.0.0.1:500x;
                auth_basic_user_file "/etc/nginx/passwd/<username>_passwd";
        }
}
```

# Bonus

## FTP

```bash
apt-get install vsftpd openssl
vim /etc/vsftpd.conf
```

```ini
local_enable=YES
write_enable=YES
local_umask=022

chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd.chroot_list

require_ssl_reuse=NO
ssl_ciphers=HIGH
ssl_enable=YES
allow_anon_ssl=NO
force_local_data_ssl=YES
force_local_logins_ssl=YES
ssl_tlsv1=YES
ssl_sslv2=NO
ssl_sslv3=NO
rsa_cert_file=<path_to_certificate>
rsa_private_key_file=<path_to_private_key>
```

```bash
openssl req -new -x509 -days 365 -nodes -out /etc/ssl/private/vsftpd.pem -keyout /etc/ssl/private/vsftpd.pem
```

```bash
chown -R <username>:<username> /home/<username>
chown root:<username> /home/<username>
```

## Nginx SSL renew

```bash
./certbot-auto renew
```

## Screen rtorrent

```bash
su <username>
script /dev/null
screen -r <username>-rtorrent
```

```bash
Ctrl+s #start download
Ctrl+d #stop an active download or remove a stopped download
Ctrl+a+d #detach screen
```