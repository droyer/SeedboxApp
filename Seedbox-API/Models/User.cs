using System.Collections.Generic;
using System.Threading;

namespace Seedbox_API.Models
{
    public class User
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public string Path { get; set; }
        public string TorrentsPath { get; set; }
        public Dictionary<string, Torrent> Torrents { get; set; }
        public SemaphoreSlim SemaphoreSlim { get; set; }
        public bool IsRestarting { get; set; }
    }
}