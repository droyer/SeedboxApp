namespace Seedbox_API.Models
{
    public class Torrent
    {
        public string Hash { get; set; }
        public string Name { get; set; }
        public double SizeBytes { get; set; }
        public double DownloadBytes { get; set; }
        public double SendBytes { get; set; }
        public double ReceptionBytes { get; set; }

        public bool Done { get { return DownloadBytes == SizeBytes; } }
    }
}