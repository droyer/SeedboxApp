namespace Seedbox_API.Dtos
{
    public class TorrentDto
    {
        public string Hash { get; set; }
        public string Name { get; set; }
        public string Size { get; set; }
        public int Percent { get; set; }
        public string Download { get; set; }
        public string Send { get; set; }
        public double Ratio { get; set; }
        public string Reception { get; set; }
        public string TimeLeft { get; set; }
    }
}