#!/usr/bin/env python
# encoding=utf8

from xmlrpclib import ServerProxy, Error
import sys

reload(sys)
sys.setdefaultencoding('utf8')

if len(sys.argv) < 4:
    sys.exit("usage: xmlrpc user password method [target]")

user = sys.argv[1]
password = sys.argv[2]
method = sys.argv[3]
target = ""
if len(sys.argv) == 5:
    target = sys.argv[4]

url = "http://{0}:{1}@127.0.0.1:8080/{2}".format(user, password, user.upper())
server = ServerProxy(url)

try:
    if method == "load.start":
        exec("print server.{0}('', '{1}')".format(method, target))
    else:
        exec("print server.{0}('{1}')".format(method, target))
except Error as v:
    print "ERROR", v
