﻿using System.Collections.Generic;
using System.IO;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Seedbox_API.Data;
using Seedbox_API.Models;

namespace Seedbox_API
{
    public class Startup
    {
        public static Dictionary<string, User> Users { get; private set; }

        public static string UsersPath { get; private set; }

        private readonly IWebHostEnvironment _hostingEnvironment;

        public Startup(IConfiguration configuration, IWebHostEnvironment hostingEnvironment)
        {
            Configuration = configuration;
            _hostingEnvironment = hostingEnvironment;

            Users = new Dictionary<string, User>();

            UsersPath = hostingEnvironment.WebRootPath + "/users/";

            if (!Directory.Exists(UsersPath))
                Directory.CreateDirectory(UsersPath);
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddCors();
            services.AddAutoMapper();
            services.AddScoped<ITorrentRepository, TorrentRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
            }

            app.UseRouting();
            app.UseCors(x => x.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseDefaultFiles();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
