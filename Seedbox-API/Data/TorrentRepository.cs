using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Seedbox_API.Helpers;
using Seedbox_API.Models;

namespace Seedbox_API.Data
{
    public class TorrentRepository : ITorrentRepository
    {
        public async Task<IEnumerable<Torrent>> GetTorrentsAsync(User user)
        {
            var torrents = new List<Torrent>();

            if (user.Torrents == null)
            {
                user.Torrents = new Dictionary<string, Torrent>();
                await user.Torrents.ReadAsync(user);
            }

            var hashes = await GetHashesAsync(user);
            if (hashes == null)
                return null;

            foreach (var hash in hashes)
            {
                Torrent torrent;
                user.Torrents.TryGetValue(hash, out torrent);
                if (torrent == null)
                {
                    torrent = new Torrent();
                    user.Torrents.Add(hash, torrent);
                }

                torrent.Hash = hash;

                var getList = new List<Task>();
                getList.Add(Task.Run(async () => { torrent.Name = await torrent.GetNameAsync(user); }));
                getList.Add(Task.Run(async () => { torrent.SizeBytes = await torrent.GetSizeBytesAsync(user); }));
                getList.Add(Task.Run(async () => { torrent.DownloadBytes = await torrent.GetDownloadBytesAsync(user); }));
                getList.Add(Task.Run(async () => { torrent.SendBytes = await torrent.GetSendBytesAsync(user); }));
                getList.Add(Task.Run(async () => { torrent.ReceptionBytes = await torrent.GetReceptionBytesAsync(user); }));
                await Task.WhenAll(getList);

                torrents.Add(torrent);
            }

            await torrents.WriteAsync(user);

            return torrents;
        }

        public async Task RemoveTorrentAsync(User user, string hash)
        {
            string path = await Utility.XmlRpcAsync(user, "d.base_path", hash);
            if (Directory.Exists(path))
                Directory.Delete(path, true);
            else if (System.IO.File.Exists(path))
                System.IO.File.Delete(path);

            await Utility.XmlRpcAsync(user, "d.erase", hash);

            user.Torrents.Remove(hash);
        }

        public async Task<Torrent> AddFileTorrentAsync(User user, IFormFile file)
        {
            if (!file.FileName.Contains(".torrent"))
                return null;

            string path = user.Path + file.FileName;
            path = path.Replace("'", "");

            using (var fs = System.IO.File.Create(path))
            {
                await file.CopyToAsync(fs);
                await fs.FlushAsync();
            }

            path = "\"" + path + "\"";

            return await NewTorrentAsync(user, path);
        }

        public async Task<Torrent> AddMagnetTorrentAsync(User user, string magnet)
        {
            return await NewTorrentAsync(user, magnet);
        }

        private async Task<Torrent> NewTorrentAsync(User user, string args)
        {
            await Utility.XmlRpcAsync(user, "load.start", args);

            var hashes = await GetHashesAsync(user);
            if (hashes == null || hashes.Length == 0)
                return null;
            string hash = hashes[hashes.Length - 1];
            if (user.Torrents.ContainsKey(hash))
                return null;

            var torrent = new Torrent();

            torrent.Hash = hash;
            torrent.Name = await torrent.GetNameAsync(user);
            torrent.SizeBytes = await torrent.GetSizeBytesAsync(user);

            user.Torrents.Add(hash, torrent);

            return torrent;
        }

        private async Task<string[]> GetHashesAsync(User user)
        {
            var hashes = await Utility.XmlRpcAsync(user, "download_list");
            if (hashes.Contains("ERROR"))
                return null;

            return hashes.Split(new[] { '[', ',', '\'', ' ', '\n', ']' }, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}