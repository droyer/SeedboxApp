using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Seedbox_API.Models;

namespace Seedbox_API.Data
{
    public interface ITorrentRepository
    {
        Task<IEnumerable<Torrent>> GetTorrentsAsync(User user);

        Task RemoveTorrentAsync(User user, string hash);

        Task<Torrent> AddFileTorrentAsync(User user, IFormFile file);

        Task<Torrent> AddMagnetTorrentAsync(User user, string magnet);
    }
}