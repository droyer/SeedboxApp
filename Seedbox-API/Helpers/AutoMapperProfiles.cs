using System;
using AutoMapper;
using Seedbox_API.Dtos;
using Seedbox_API.Models;

namespace Seedbox_API.Helpers
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Torrent, TorrentDto>()
                .ForMember(dest => dest.Size, opt => opt.MapFrom(src => GetSize(src)))
                .ForMember(dest => dest.Percent, opt => opt.MapFrom(src => GetPercent(src)))
                .ForMember(dest => dest.Download, opt => opt.MapFrom(src => GetDownload(src)))
                .ForMember(dest => dest.Send, opt => opt.MapFrom(src => GetSend(src)))
                .ForMember(dest => dest.Ratio, opt => opt.MapFrom(src => GetRatio(src)))
                .ForMember(dest => dest.Reception, opt => opt.MapFrom(src => GetReception(src)))
                .ForMember(dest => dest.TimeLeft, opt => opt.MapFrom(src => GetTimeLeft(src)));
        }

        private string GetSize(Torrent torrent)
        {
            return Utility.FormatBytes((long)torrent.SizeBytes);
        }

        private int GetPercent(Torrent torrent)
        {
            if (torrent.DownloadBytes > torrent.SizeBytes)
                return 0;

            return (int)Math.Floor(torrent.DownloadBytes / torrent.SizeBytes * 100.0);
        }

        private string GetDownload(Torrent torrent)
        {
            return Utility.FormatBytes((long)torrent.DownloadBytes);
        }

        private string GetSend(Torrent torrent)
        {
            return Utility.FormatBytes((long)torrent.SendBytes);
        }

        private string GetRatio(Torrent torrent)
        {
            return (torrent.SendBytes / torrent.SizeBytes).ToString("0.#");
        }

        private string GetReception(Torrent torrent)
        {
            return Utility.FormatBytes((long)torrent.ReceptionBytes) + "/s";
        }

        private string GetTimeLeft(Torrent torrent)
        {
            if (torrent.Done)
                return "∞";

            if (torrent.SizeBytes == 0 || torrent.ReceptionBytes == 0)
                return "?";

            double sec = (torrent.SizeBytes - torrent.DownloadBytes) / torrent.ReceptionBytes;

            var span = TimeSpan.FromSeconds(sec);
            string time = "";
            if (span.Days > 0)
                time += span.Days + "j ";
            if (span.Hours > 0)
                time += span.Hours + "h ";
            if (span.Minutes > 0)
                time += span.Minutes + "m ";
            if (span.Seconds > 0)
                time += span.Seconds + "s ";
            if (string.IsNullOrWhiteSpace(time))
                return "?";
            time = time.Remove(time.Length - 1);
            return time;
        }
    }
}