using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Seedbox_API.Models;

namespace Seedbox_API.Helpers
{
    public static class HttpContextExtensions
    {
        public static async Task<User> GetUserAsync(this HttpContext context)
        {
            string auth = context.Request.Headers["Authorization"];

            if (!Startup.Users.ContainsKey(auth))
                return await Task.Run(() => context.CreateUser(auth));

            return Startup.Users[auth];
        }

        private static User CreateUser(this HttpContext context, string auth)
        {
            var user = new User();

            string tmp = auth.Replace("Basic ", "");
            tmp = Utility.Base64Decode(tmp);
            var data = tmp.Split(':');
            user.Name = data[0];
            user.Password = data[1];
            user.Path = Startup.UsersPath + user.Name + "/";
            if (!Directory.Exists(user.Path))
                Directory.CreateDirectory(user.Path);
            user.TorrentsPath = user.Path + "torrents";
            user.SemaphoreSlim = new SemaphoreSlim(1, 1);
            Startup.Users.Add(auth, user);

            return user;
        }
    }
}