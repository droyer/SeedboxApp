using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Seedbox_API.Models;

namespace Seedbox_API.Helpers
{
    public static class TorrentExtensions
    {
        public static async Task ReadAsync(this Dictionary<string, Torrent> torrents, User user)
        {
            if (!System.IO.File.Exists(user.TorrentsPath))
                return;

            using (var sr = new StreamReader(user.TorrentsPath))
            {
                int count = Convert.ToInt32(await sr.ReadLineAsync());

                for (int i = 0; i < count; i++)
                {
                    var torrent = new Torrent
                    {
                        Hash = await sr.ReadLineAsync(),
                        Name = await sr.ReadLineAsync(),
                        SizeBytes = Convert.ToDouble(await sr.ReadLineAsync()),
                        DownloadBytes = Convert.ToDouble(await sr.ReadLineAsync()),
                    };

                    torrents.Add(torrent.Hash, torrent);
                }

                sr.Close();
            }
        }

        public static async Task WriteAsync(this List<Torrent> torrents, User user)
        {
            using (var sw = new StreamWriter(user.TorrentsPath))
            {
                await sw.WriteLineAsync(torrents.Count.ToString());

                foreach (var torrent in torrents)
                {
                    await sw.WriteLineAsync(torrent.Hash);
                    await sw.WriteLineAsync(torrent.Name);
                    await sw.WriteLineAsync(torrent.SizeBytes.ToString());
                    await sw.WriteLineAsync(torrent.DownloadBytes.ToString());
                }

                sw.Close();
            }
        }

        public static async Task<string> GetNameAsync(this Torrent torrent, User user)
        {
            if (string.IsNullOrEmpty(torrent.Name) || torrent.Name.Contains(".meta") || torrent.Name.Contains("ERROR"))
                return await Utility.XmlRpcAsync(user, "d.name", torrent.Hash);

            return torrent.Name;
        }

        public static async Task<double> GetSizeBytesAsync(this Torrent torrent, User user)
        {
            if (torrent.SizeBytes == 0 || torrent.DownloadBytes > torrent.SizeBytes)
            {
                double result;
                if (double.TryParse(await Utility.XmlRpcAsync(user, "d.size_bytes", torrent.Hash), out result))
                    return result;
            }

            return torrent.SizeBytes;
        }

        public static async Task<double> GetDownloadBytesAsync(this Torrent torrent, User user)
        {
            if (torrent.DownloadBytes == 0 || !torrent.Done)
            {
                double result;
                if (double.TryParse(await Utility.XmlRpcAsync(user, "d.completed_bytes", torrent.Hash), out result))
                    return result;
            }

            return torrent.DownloadBytes;
        }

        public static async Task<double> GetSendBytesAsync(this Torrent torrent, User user)
        {
            double result;
            if (double.TryParse(await Utility.XmlRpcAsync(user, "d.up.total", torrent.Hash), out result))
                return result;

            return 0;

        }

        public static async Task<double> GetReceptionBytesAsync(this Torrent torrent, User user)
        {
            if (!torrent.Done)
            {
                double result;
                if (double.TryParse(await Utility.XmlRpcAsync(user, "d.down.rate", torrent.Hash), out result))
                    return result;
            }

            return 0;
        }
    }
}